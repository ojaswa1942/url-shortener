# url-shortener

A service to shorten urls, like goo.gl and bit.ly

### INSTALLATION

`npm install`

### USAGE

`npm start`

#### RUNNING ONLY REACT FRONT-END

`npm run start-react`
